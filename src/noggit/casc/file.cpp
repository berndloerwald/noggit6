// file.cpp is part of Noggit3, licensed via GNU General Public License (version 3).
// Bernd Lörwald <bloerwald+noggit@googlemail.com>
// Mjollnà <mjollna.wow@gmail.com>

#include <noggit/casc/file.h>

#include <stdexcept>

#include <CascLib.h>

#include <noggit/Log.h>
#include <noggit/casc/storage.h>
#include <noggit/application.h>
#include <helper/qt/case_insensitive.h>

namespace noggit
{
  namespace casc
  {
    helper::qt::case_insensitive::directory file::_disk_search_path;

    file::file (const QString& filename, const bool& maybe_create)
      : _is_at_end_of_file (true)
      , buffer (NULL)
      , pointer (0)
      , size (0)
      , _file_is_on_disk (false)
      , _filename (filename)
    {
      if (!exists (_filename))
      {
        if (!maybe_create)
        {
          LogError << "Requested file "
                   << qPrintable (_filename)
                   << " which does not exist."
                   << std::endl;

          throw std::runtime_error ("Requested file does not exist.");
        }
        else
        {
          _file_is_on_disk = true;
        }
      }
      else
      {
        if (_disk_search_path.exists (_filename))
        {
          helper::qt::case_insensitive::file file (_disk_search_path.absoluteFilePath (_filename));
          file.open (QFile::ReadOnly);

          size = file.size();
          buffer = new char[size];

          memcpy (buffer, file.readAll().data(), size);
        }
        else
        {
          app().casc_storage().open_file (_filename, &size, &buffer);
        }
      }

      _is_at_end_of_file = size == 0;
    }

    void file::disk_search_path
      (const helper::qt::case_insensitive::directory& path)
    {
      _disk_search_path = path;
    }

    file::~file()
    {
      close();
    }

    bool file::exists (const QString &filename)
    {
      return app().casc_storage().has_file (filename)
          || _disk_search_path.exists (filename);
    }

    size_t file::read (void* dest, size_t bytes)
    {
      if (_is_at_end_of_file)
        return 0;

      size_t rpos (pointer + bytes);
      if (rpos > size)
      {
        bytes = size - pointer;
        _is_at_end_of_file = true;
      }

      memcpy (dest, &(buffer[pointer]), bytes);

      pointer = rpos;

      return bytes;
    }

    bool file::is_at_end_of_file() const
    {
      return _is_at_end_of_file;
    }

    void file::seek(size_t offset)
    {
      pointer = offset;
      _is_at_end_of_file = (pointer >= size);
    }

    void file::seekRelative(size_t offset)
    {
      pointer += offset;
      _is_at_end_of_file = (pointer >= size);
    }

    void file::close()
    {
      delete[] buffer;
      buffer = NULL;

      _is_at_end_of_file = true;
    }

    size_t file::getSize() const
    {
      return size;
    }

    size_t file::getPos() const
    {
      return pointer;
    }

    bool file::file_is_on_disk() const
    {
      return _file_is_on_disk;
    }

    char* file::getBuffer() const
    {
      return buffer;
    }

    void file::setBuffer(char *Buf, size_t Size)
    {
      delete buffer;
      buffer = NULL;

      buffer=Buf;
      size=Size;
    }

    char* file::getPointer() const
    {
      return buffer + pointer;
    }

    void file::save_to_disk()
    {
      helper::qt::case_insensitive::directory const dir
        ( _disk_search_path.absoluteFilePath
            (_filename.left (_filename.lastIndexOf (QRegExp ("[\\\\/]"))))
        );

      if (!QDir().mkpath (dir.absolutePath()))
      {
        LogError << "Saving failed: Unable to create directory \""
                 << qPrintable (dir.absolutePath())
                 << "\"." << std::endl;
        return;
      }

      helper::qt::case_insensitive::file output_file
        (_disk_search_path.absoluteFilePath (_filename));

      if (!output_file.open (QFile::WriteOnly))
      {
        LogError << "Saving failed: Unable to open \""
                 << qPrintable (output_file.fileName())
                 << "\" for writing." << std::endl;
        return;
      }

      Log << "Saving file \""
          << qPrintable (output_file.fileName())
          << "\"."
          << std::endl;

      output_file.write (buffer, size);

      _file_is_on_disk = true;
    }
  }
}

