// storage.cpp is part of Noggit3, licensed via GNU General Public License (version 3).
// Bernd Lörwald <bloerwald+noggit@googlemail.com>

#include <noggit/casc/storage.h>

#include <CascLib.h>

#include <noggit/casc/file.h>
#include <noggit/Log.h>
#include <noggit/application.h>

#include <QStringList>

#include <stdexcept>

namespace noggit
{
  namespace casc
  {
    storage::storage (const QDir& data_path, bool process_list_file)
      : _storage_handle (NULL)
    {
      //! \todo correctly detect and use locales
      if ( !CascOpenStorage ( data_path.canonicalPath().toLatin1().data()
                            , CASC_LOCALE_DEDE
                            , &_storage_handle
                            )
           )
      {
          LogError << "Error opening storage: " << data_path.canonicalPath().toStdString() << "\n";
          throw std::runtime_error ("Opening storage failed.");
      }
      else
      {
          LogDebug << "Opened storage " << data_path.canonicalPath().toStdString() << "\n";
      }


      _finished = !process_list_file;
    }

    namespace
    {
      //! \note This all does not work with files > 2^32 at all.
      std::size_t get_file_size (HANDLE file_handle)
      {
        unsigned int filesize_high (0);
        const unsigned int filesize_low (CascGetFileSize (file_handle, &filesize_high));
        return filesize_low | size_t (filesize_high) << 32;
      }

      void read_file (HANDLE file_handle, void* buffer, std::size_t size)
      {
        unsigned int read (0);
        CascReadFile (file_handle, buffer, size, &read);
        if (read != size)
        {
          throw std::logic_error ("read less than filesize");
        }
      }
    }

    void storage::finish_loading()
    {
      if (_finished)
        return;

      _finished = true;

      HANDLE file_handle;

      //! \todo Restore something listfile-y.
      // if (CascOpenFile (_storage_handle, "(listfile)", CASC_LOCALE_DEDE, 0, &file_handle))
      // {
      //   const size_t filesize (get_file_size (file_handle));

      //   char* readbuffer (new char[filesize]);
      //   read_file (file_handle, readbuffer, filesize);
      //   CascCloseFile (file_handle);
      //   _listfile = QString::fromAscii (readbuffer, filesize).toLower().split ("\r\n", QString::SkipEmptyParts);

      //   delete[] readbuffer;
      // }
    }

    const QStringList& storage::listfile() const
    {
      return _listfile;
    }


    storage::~storage()
    {
      if (_storage_handle)
      {
        CascCloseStorage (_storage_handle);
        _storage_handle = NULL;
      }
    }

    bool storage::has_file (QString filename) const
    {
      HANDLE file_handle;
      if ( !CascOpenFile ( _storage_handle
                         , qPrintable (filename)
                         , CASC_LOCALE_DEDE
                         , 0
                         , &file_handle
                         )
         )
      {
        return false;
      }
      return CascCloseFile (file_handle);
    }

    bool storage::open_file ( const QString& filename
                            , size_t* size
                            , char** buffer
                            ) const
    {
      HANDLE file_handle;
      bool opened ( CascOpenFile ( _storage_handle
                                 , qPrintable (filename)
                                 , CASC_LOCALE_DEDE
                                 , 0
                                 , &file_handle
                                 )
                  );
      if (opened)
      {
        *size = get_file_size (file_handle);
        *buffer = new char[*size];

        read_file (file_handle, *buffer, *size);
        CascCloseFile (file_handle);
      }

      return opened;
    }
  }
}
