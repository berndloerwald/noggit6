// storage.h is part of Noggit3, licensed via GNU General Public License (version 3).
// Bernd Lörwald <bloerwald+noggit@googlemail.com>

#ifndef __NOGGIT_MPQ_STORAGE_H
#define __NOGGIT_MPQ_STORAGE_H

#include <QDir>
#include <QString>
#include <QStringList>

#include <noggit/async/object.h>

#include <boost/noncopyable.hpp>

namespace noggit
{
  class application;
  namespace casc
  {
    //! \note Instead of including StormLib.
    typedef void* HANDLE;

    class file;

    class storage : public async::object, boost::noncopyable
    {
    public:
      storage (const QDir& data_path, bool process_list_file);
      ~storage();

      bool has_file (QString filename) const;
      bool open_file ( const QString& filename
                     , size_t* size
                     , char** buffer
                     ) const;

      void finish_loading();
      const QStringList& listfile() const;

    private:
      HANDLE _storage_handle;
      QStringList _listfile;

      friend class file;
      friend class noggit::application;
    };
  }
}

#endif

